﻿using Food.Api.Contracts;
using Food.Api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Food.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;

        public AccountsController(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }


        [HttpPost("/login")]
        public async Task<ActionResult<TokenModel>> Login(LoginModel model)
        {
            var isCorrect = await _authRepository.IsLoginAccesCorrectAsync(model);

            if (!isCorrect)
                return Unauthorized();

            return Ok(await _authRepository.LoginAsync(model));
        }


        [HttpPost("/register")]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            var result = await _authRepository.RegisterAsync(model);

            if (!result.Succeeded)
                return BadRequest(result.Errors);

            return StatusCode(201);
        }

    }
}
