﻿using Food.Api.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Food.Api.Contracts
{
    public interface IAuthRepository
    {
        Task<TokenModel> LoginAsync(LoginModel model);
        Task<bool> IsLoginAccesCorrectAsync(LoginModel model);
        Task<IdentityResult> RegisterAsync(RegisterModel model);
        Task LogoutAsync();
    }
}
