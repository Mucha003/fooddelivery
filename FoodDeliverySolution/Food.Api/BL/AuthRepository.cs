﻿using Food.Api.Contracts;
using Food.Api.DAL;
using Food.Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Food.Api.BL
{
    public class AuthRepository : IAuthRepository
    {
        private readonly SettingsConfig _settingsConfig;
        private readonly FoodDeliveryContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AuthRepository(
            FoodDeliveryContext context,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IOptions<SettingsConfig> appSettings)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _settingsConfig = appSettings.Value;
        }


        public async Task<TokenModel> LoginAsync(LoginModel model)
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_settingsConfig.SecretKeyToken);

            var user = await _userManager.FindByNameAsync(model.UserName);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Name, user.UserName)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var encryptToken = tokenHandler.WriteToken(token);

            return new TokenModel()
            {
                AccessToken = encryptToken
            };
        }


        public async Task<IdentityResult> RegisterAsync(RegisterModel model)
        {
            var user = new User
            {
                Email = model.Email,
                UserName = model.UserName,
                Firstname = model.FirstName,
                Lastname = model.LastName,
            };

            return await _userManager.CreateAsync(user, model.Password);
        }


        public async Task LogoutAsync()
        {
            await _signInManager.SignOutAsync();
        }


        public async Task<bool> IsLoginAccesCorrectAsync(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user == null)
                return false;

            bool passValid = await _userManager.CheckPasswordAsync(user, model.Password);

            if (!passValid)
                return false;

            return true;
        }

    }
}
