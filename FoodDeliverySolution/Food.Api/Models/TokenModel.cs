﻿using System;

namespace Food.Api.Models
{
    public class TokenModel
    {
        public string AccessToken { get; set; }
    }
}
