﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Food.Api.Models
{
    public class SettingsConfig
    {
        public string SecretKeyToken { get; set; }
    }
}
