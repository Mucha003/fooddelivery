﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Food.Api.DAL
{
    public class User : IdentityUser
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
