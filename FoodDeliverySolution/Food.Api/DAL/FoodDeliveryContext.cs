﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Food.Api.DAL
{
    public class FoodDeliveryContext : IdentityDbContext<User>
    {
        public FoodDeliveryContext(DbContextOptions<FoodDeliveryContext> opt)
            : base(opt)
        {
        }


        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }

    }
}
