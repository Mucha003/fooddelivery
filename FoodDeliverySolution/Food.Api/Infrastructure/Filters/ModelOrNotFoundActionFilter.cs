﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Food.Api.Infrastructure.Filters
{
    // return not found Api
    // dont forget to add to serviceCollection controller options.
    public class ModelOrNotFoundActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Result is ObjectResult result)
            {
                var model = result.Value;

                if (model == null)
                {
                    context.Result = new NotFoundResult();
                }
            }
        }


    }
}
