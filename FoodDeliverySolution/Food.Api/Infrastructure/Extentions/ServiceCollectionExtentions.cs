﻿using Food.Api.DAL;
using Food.Api.Infrastructure.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

namespace Food.Api.Infrastructure.Extentions
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddDataBaseExtention(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<FoodDeliveryContext>(options
                    => options.UseSqlServer(Configuration.GetDefaultConnectionString()));

            return services;
        }


        public static IServiceCollection AddIdentityExtention(this IServiceCollection services)
        {
            services
                .AddIdentity<User, IdentityRole>(opt =>
                {
                    opt.Password.RequiredLength = 2;
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireLowercase = false;
                    opt.Password.RequireUppercase = false;
                    opt.Password.RequireNonAlphanumeric = false;
                })
                .AddEntityFrameworkStores<FoodDeliveryContext>();

            return services;
        }


        public static IServiceCollection AddSwaggerExtention(this IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                              new OpenApiInfo
                              {
                                  Title = "Food Delivery API",
                                  Version = "v1"
                              });
            });

            return services;
        }


        public static IServiceCollection AddJWTAuthenticationExtention(this IServiceCollection services, IConfiguration Configuration)
        {
            // get Data From JsonSetting
            var appSettings = services.GetAppSettings(Configuration);

            // ------------------       JWT       ------------------     
            // configure jwt authentication
            var key = Encoding.ASCII.GetBytes(appSettings.SecretKeyToken);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            // ------------------       End - JWT       ------------------

            return services;
        }


        public static void AddApiControllers(this IServiceCollection services)
        {
            services.AddControllers(options => options.Filters.Add<ModelOrNotFoundActionFilter>());
        }

    }
}
