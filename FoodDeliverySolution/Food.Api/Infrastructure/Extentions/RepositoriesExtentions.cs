﻿using Food.Api.BL;
using Food.Api.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Food.Api.Infrastructure.Extentions
{
    public static class RepositoriesExtentions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IAuthRepository, AuthRepository>();

            return services;
        }

    }
}
