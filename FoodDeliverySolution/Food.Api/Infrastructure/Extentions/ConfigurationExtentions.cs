﻿using Food.Api.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Food.Api.Infrastructure.Extentions
{
    public static class ConfigurationExtentions
    {
        public static string GetDefaultConnectionString(this IConfiguration Configuration)
        {
            return Configuration.GetConnectionString("DefaultConnection");
        }


        public static SettingsConfig GetAppSettings(this IServiceCollection services, IConfiguration Configuration)
        {
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("SettingsConfig");
            services.Configure<SettingsConfig>(appSettingsSection);

            return appSettingsSection.Get<SettingsConfig>();
        }


    }
}
