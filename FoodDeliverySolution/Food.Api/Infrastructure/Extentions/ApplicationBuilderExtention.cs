﻿using Food.Api.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Food.Api.Infrastructure.Extentions
{
    public static class ApplicationBuilderExtention
    {
        public static void ApplySwaggerUI(this IApplicationBuilder app)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger()
               .UseSwaggerUI(opt =>
               {
                   // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
                   // specifying the Swagger JSON endpoint.
                   opt.SwaggerEndpoint("/swagger/v1/swagger.json", "Food Delivery API");
                   opt.RoutePrefix = string.Empty;
               });
        }


        public static void ApplyCors(this IApplicationBuilder app)
        {
            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        }


        public static void ApplyMigrations(this IApplicationBuilder app)
        {
            using var service = app.ApplicationServices.CreateScope();

            var dbContext = service.ServiceProvider.GetService<FoodDeliveryContext>();
            dbContext.Database.Migrate();
        }


    }
}
